package go;

import app.views.Login;
import app.views.DenominationBreakdown;
import app.views.CaptureDenomination;
import app.listeners.ComponentListDenominationBreakdown;
import app.listeners.ComponentListCaptureDenomination;
import java.awt.CardLayout;
import javax.swing.WindowConstants;

/**
 *
 * @author ryan
 */
public class GO extends javax.swing.JFrame {
    
    private javax.swing.JLabel lbAppName;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private String appName = "Cash Dispense System";
    
    private CardLayout cardLayout;

    
    public GO() {
        initComponents();
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GO().setVisible(true);
            }
        });
        
    }

    private void initComponents() {
        lbAppName = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        
        lbAppName.setText(appName);
        
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addGap(109, 109, 109)
                .addComponent(lbAppName)
                .addContainerGap(130, Short.MAX_VALUE))
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbAppName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        
        cardLayout = new CardLayout();
        jPanel1.setLayout(cardLayout);
        CaptureDenomination captureDenomination = new CaptureDenomination();
        captureDenomination.addComponentListener(new ComponentListCaptureDenomination());
        DenominationBreakdown denominationBreakdownResult = new DenominationBreakdown();
        denominationBreakdownResult.addComponentListener(new ComponentListDenominationBreakdown());;
        jPanel1.add(new Login(), "Login");
        jPanel1.add(captureDenomination, "CaptureDenomination");
        jPanel1.add(denominationBreakdownResult, "DenominationBreakdownResult");
        
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        
        pack();
    }
    
}
