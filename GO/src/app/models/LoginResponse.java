package app.models;

import app.models.UserModel;

/**
 *The login response from the rest request
 * 
 * @author ryan
 */
public class LoginResponse {
    private boolean success = false;
    private UserModel userModel;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public UserModel getUserModel() {
        return userModel;
    }
    
    public void setUserModel(UserModel user) {
        this.userModel = userModel;
    }
    
    
    
    
    
}
