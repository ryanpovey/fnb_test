package app.models;

import java.util.List;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

/**
 * Class to hold session properties
 * 
 * @author ryan
 */
public class Session {

    /**
     * @return the breakDown
     */
    public static List<String> getBreakDown() {
        return breakDown;
    }

    /**
     * @param aBreakDown the breakDown to set
     */
    public static void setBreakDown(List<String> aBreakDown) {
        breakDown = aBreakDown;
    }

    /**
     * @return the userId
     */
    public static String getUserId() {
        return userId;
    }

    /**
     * @param aUserId the userId to set
     */
    public static void setUserId(String aUserId) {
        userId = aUserId;
    }

    /**
     * @return the balance
     */
    public static BigDecimal getBalance() {
        return balance;
    }

    /**
     * @param aBalance the balance to set
     */
    public static void setBalance(BigDecimal aBalance) {
        balance = aBalance;
    }

    /**
     * @return the dispenseAmount
     */
    public static BigDecimal getDispenseAmount() {
        return dispenseAmount;
    }

    /**
     * @param aDispenseAmount the dispenseAmount to set
     */
    public static void setDispenseAmount(BigDecimal aDispenseAmount) {
        dispenseAmount = aDispenseAmount;
    }
    
    public static void clear() {
        userId = "";
        dispenseAmount = new  BigDecimal(BigInteger.ZERO);
        balance = new BigDecimal(BigInteger.ZERO);
        breakDown = new ArrayList<>();
    }
    
    private static String userId;
    private static BigDecimal dispenseAmount;
    private static BigDecimal balance;
    private static List<String> breakDown;
    
}
