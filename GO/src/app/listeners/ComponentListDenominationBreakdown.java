package app.listeners;

import app.models.Session;
import java.awt.Component;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *A component listener for the denominationBreaqkDown panel
 * 
 * @author ryan
 */
public class ComponentListDenominationBreakdown implements ComponentListener {

    @Override
    public void componentResized(ComponentEvent e) {
    }

    @Override
    public void componentMoved(ComponentEvent e) {
    }

    /**
     * When the panel is shown set the labels to the correct values based on the List of break down amounts
     * 
     * @param e 
     */
    @Override
    public void componentShown(ComponentEvent e) {
        //Drilling down the components to find the placeholder labels to populate with the given amounts
        JPanel v = (JPanel) e.getComponent();
        Component a = e.getComponent();
        for (Component jP : v.getComponents()) {
            //looking for grid layout panel
            if (jP instanceof JPanel) {
                JPanel v1 = (JPanel) jP;
                //looking for labels in grif layour panel
                for (Component jc : v1.getComponents()) {
                    if (jc instanceof JLabel && jc.getName() != null) {
                        //Get the break down list from the session properties
                        List<String> breakDown = Session.getBreakDown();
                        //populate the corresponding labels based on the amount of denomination needed
                        if (jc.getName().equals("place1")) {
                            JLabel place1 = (JLabel) jc;
                            if (breakDown.get(0) != null) {
                                place1.setText(breakDown.get(0));
                            } else {
                                place1.setText(" ");
                            }
                        } else if (jc.getName().equals("place5")) {
                            JLabel place5 = (JLabel) jc;
                            if (breakDown.size() > 1) {
                                place5.setText(breakDown.get(1));
                            } else {
                                place5.setText(" ");
                            }
                        } else if (jc.getName().equals("place9")) {
                            JLabel place9 = (JLabel) jc;
                            if (breakDown.size() > 2) {
                                place9.setText(breakDown.get(2));
                            } else {
                                place9.setText(" ");
                            }
                        } else if (jc.getName().equals("place2")) {
                            JLabel place2 = (JLabel) jc;
                            if (breakDown.size() > 3) {
                                place2.setText(breakDown.get(3));
                            } else {
                                place2.setText(" ");
                            }
                        } else if (jc.getName().equals("place6")) {
                            JLabel place6 = (JLabel) jc;
                            if (breakDown.size() > 4) {
                                place6.setText(breakDown.get(4));
                            } else {
                                place6.setText(" ");
                            }
                        } else if (jc.getName().equals("place10")) {
                            JLabel place10 = (JLabel) jc;
                            if (breakDown.size() > 5) {
                                place10.setText(breakDown.get(5));
                            } else {
                                place10.setText(" ");
                            }
                        } else if (jc.getName().equals("place3")) {
                            JLabel place3 = (JLabel) jc;
                            if (breakDown.size() > 6) {
                                place3.setText(breakDown.get(6));
                            } else {
                                place3.setText("");
                            }
                        } else if (jc.getName().equals("place7")) {
                            JLabel place7 = (JLabel) jc;
                            if (breakDown.size() > 7) {
                                place7.setText(breakDown.get(7));
                            } else {
                                place7.setText(" ");
                            }
                        } else if (jc.getName().equals("place11")) {
                            JLabel place11 = (JLabel) jc;
                            if (breakDown.size() > 8) {
                                place11.setText(breakDown.get(8));
                            } else {
                                place11.setText(" ");
                            }
                        } else if (jc.getName().equals("place4")) {
                            JLabel place4 = (JLabel) jc;
                            if (breakDown.size() > 9) {
                                place4.setText(breakDown.get(9));
                            } else {
                                place4.setText(" ");
                            }
                        } else if (jc.getName().equals("place8")) {
                            JLabel place8 = (JLabel) jc;
                            if (breakDown.size() > 10) {
                                place8.setText(breakDown.get(10));
                            } else {
                                place8.setText(" ");
                            }
                        }

                    }
                }
            } else if (jP instanceof JLabel && jP.getName() != null) {
                if (jP.getName().equals("dispenseTotal")) {
                    JLabel total = (JLabel) jP;
                    total.setText("R" + Session.getDispenseAmount().toPlainString());
                }

            }
        }
    }

    @Override
    public void componentHidden(ComponentEvent e) {
    }
}
