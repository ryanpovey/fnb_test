package app.listeners;

import app.models.Session;
import java.awt.Component;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.text.NumberFormat;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *A component listener for the capturedenomination panel
 * 
 * @author ryan
 */
public class ComponentListCaptureDenomination implements ComponentListener {

    @Override
    public void componentResized(ComponentEvent e) {
    }

    @Override
    public void componentMoved(ComponentEvent e) {
    }

    /**
     * When the panel is shown set the user balance label to the current balance
     * 
     * @param e 
     */
    @Override
    public void componentShown(ComponentEvent e) {
        JPanel v = (JPanel) e.getComponent();
        for (Component jc : v.getComponents()) {
            if (jc instanceof JLabel && jc.getName() != null && jc.getName().equals("userBalance")) {
                JLabel label = (JLabel) jc;
                label.setText(NumberFormat.getCurrencyInstance().format(Session.getBalance()));
            } else if (jc instanceof JTextField) {
                JTextField amount = (JTextField) jc;
                amount.setText("");
            }
        }
    }

    @Override
    public void componentHidden(ComponentEvent e) {
    }
}
