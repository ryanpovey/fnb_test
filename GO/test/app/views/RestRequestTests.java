package app.views;

import app.models.CalcBreakDownResponse;
import app.models.UserModel;
import java.math.BigDecimal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ryan
 */
public class RestRequestTests {
    
    public RestRequestTests() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testLoginRest() {
        String user = "ryan";
        String pass = "pass";
        
        String expectedUser = "ryan";
        String expectedUserId = "00001";
        BigDecimal expectedBalance = new BigDecimal("100.00");
        
        Login login = new Login();
        UserModel userModel = login.loginRequest(user, pass);
        assertEquals(expectedUser, userModel.getUser());
        assertEquals(expectedUserId, userModel.getUserId());
        assertEquals(expectedBalance, userModel.getBalance());
    }
    
    @Test
    public void testCalcBreakDownRest() {
        String userId = "00001";
        
        String expectedResult = "1 x R10";
        
        CaptureDenomination capt = new CaptureDenomination();
        CalcBreakDownResponse response = capt.calcBreakDownRequest(userId, BigDecimal.TEN);
        assertTrue(response.isSuccess());
        assertEquals(expectedResult, response.getBreakDown().get(0));
    }
    
}
