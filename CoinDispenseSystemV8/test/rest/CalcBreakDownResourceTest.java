package rest;

import java.math.BigDecimal;
import java.math.BigInteger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import test.TestBase;

/**
 *
 * @author ryan
 */
public class CalcBreakDownResourceTest extends TestBase {

    public CalcBreakDownResourceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        insertTestUser();
    }

    @After
    public void tearDown() {
        deleteTestUser();
    }

    /**
     * Test of calcBreakDown method, of class CalcBreakDownResource.
     */
    @Test
    public void testCalcBreakDownLeftOverBalanceZero() {
        System.out.println("calcBreakDown");
        String userId = "00TEST";
        BigDecimal dispense = new BigDecimal(BigInteger.TEN);
        BigDecimal balance = new BigDecimal(BigInteger.TEN);
        CalcBreakDownResource instance = new CalcBreakDownResource();
        String expResult = "{\"success\":true,\"breakDown\":[\"1 x R10\"]}";
        String result = instance.calcBreakDown(userId, dispense);
        assertEquals(expResult, result);
    }
    
    
    /**
     * Test of calcBreakDown method, of class CalcBreakDownResource.
     */
    @Test
    public void testCalcBreakDownInputsEmpty() {
        System.out.println("calcBreakDown");
        String userId = "";
        BigDecimal dispense = null;
        BigDecimal balance = null;
        CalcBreakDownResource instance = new CalcBreakDownResource();
        String expResult = "{\"success\":false}";
        String result = instance.calcBreakDown(userId, dispense);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of calcBreakDown method, of class CalcBreakDownResource.
     */
    @Test
    public void testCalcBreakDownInsufficientFunds() {
        System.out.println("calcBreakDown");
        String userId = "00TEST";
        BigDecimal dispense = new BigDecimal(150);
        BigDecimal balance = new BigDecimal(100);
        CalcBreakDownResource instance = new CalcBreakDownResource();
        String expResult = "{\"success\":false,\"error\":\"Insufficient funds\"}";
        String result = instance.calcBreakDown(userId, dispense);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of calcBreakDown method, of class CalcBreakDownResource.
     */
    @Test
    public void testCalcBreakDownRandsAndCents() {
        System.out.println("calcBreakDown");
        String userId = "00TEST";
        BigDecimal dispense = new BigDecimal(99.75);
        CalcBreakDownResource instance = new CalcBreakDownResource();
        String expResult = "{\"success\":true,\"breakDown\":[\"1 x R50\",\"2 x R20\",\"1 x R5\",\"2 x R2\",\"1 x 50c\",\"1 x 25c\"]}";
        String result = instance.calcBreakDown(userId, dispense);
        assertEquals(expResult, result);
    }
    
    
    
    /**
     * Test of calcBreakDown method, of class CalcBreakDownResource.
     */
    @Test
    public void testCalcBreakDownRandsAndCents2() {
        System.out.println("calcBreakDown");
        String userId = "00TEST";
        BigDecimal dispense = new BigDecimal(99.95);
        CalcBreakDownResource instance = new CalcBreakDownResource();
        String expResult = "{\"success\":true,\"breakDown\":[\"1 x R50\",\"2 x R20\",\"1 x R5\",\"2 x R2\",\"1 x 50c\",\"1 x 25c\",\"2 x 10c\"]}";
        String result = instance.calcBreakDown(userId, dispense);
        assertEquals(expResult, result);
    }

    
        /**
     * Test of calcBreakDown method, of class CalcBreakDownResource.
     */
    @Test
    public void testCalcBreakDownInvalidUser() {
        System.out.println("calcBreakDown");
        String userId = "00TES2T";
        BigDecimal dispense = new BigDecimal(99.95);
        CalcBreakDownResource instance = new CalcBreakDownResource();
        String expResult = "{\"success\":false,\"error\":\"Invalid user\"}";
        String result = instance.calcBreakDown(userId, dispense);
        System.out.println("calcBreakDown" + result);
        assertEquals(expResult, result);
    }
    
}
