package rest;

import db.Database;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import test.TestBase;

/**
 * Test the login method
 *
 *
 * @author ryan
 */
public class LoginResourceTest extends TestBase {

    public LoginResourceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        insertTestUser();
    }

    @After
    public void tearDown() {
        deleteTestUser();
    }

    /**
     * Valid login test
     */
    @Test
    public void testLogin() {
        System.out.println("login");
        String user = "testLogin";
        String pass = "pass";
        LoginResource instance = new LoginResource();
        String expResult = "{\"success\":true,\"userModel\":{\"userId\":\"00TEST\",\"user\":\"testLogin\",\"balance\":1.0}}";
        String result = instance.login(user, pass);
        assertEquals(expResult, result);
    }

    @Test
    public void testLoginInValid() {
        System.out.println("login");
        String user = "test";
        String pass = "pass";
        LoginResource instance = new LoginResource();
        String expResult = "{\"success\":false}";
        String result = instance.login(user, pass);
        assertEquals(expResult, result);
    }

    @Test
    public void testLoginEmptyParameters() {
        System.out.println("login");
        String user = "";
        String pass = "";
        LoginResource instance = new LoginResource();
        String expResult = "{\"success\":false}";
        String result = instance.login(user, pass);
        assertEquals(expResult, result);
    }

    @Test
    public void testLoginNullParameters() {
        System.out.println("login");
        LoginResource instance = new LoginResource();
        String expResult = "{\"success\":false}";
        String result = instance.login(null, null);
        assertEquals(expResult, result);
    }

    @Test
    public void testLoginValidUserInValidPassword() {
        System.out.println("login");
        String user = "testLogin";
        String pass = "passwrong";
        LoginResource instance = new LoginResource();
        String expResult = "{\"success\":false}";
        String result = instance.login(null, null);
        assertEquals(expResult, result);
    }

}
