package test;

import db.Database;

/**
 *
 * @author ryan
 */
public class TestBase {

    protected static boolean insertTestUser() {
        boolean pass = false;
        Database db = new Database();
        db.connectDB();
        pass = db.insertTestUser();
        db.closeDB();
        return pass;
    }

    protected static boolean deleteTestUser() {
        boolean pass = false;
        Database db = new Database();
        db.connectDB();
        pass = db.deleteTestUser();
        db.closeDB();
        return pass;
    }

}
