package rest;

import db.Database;
import models.LoginResponse;
import models.UserModel;
import com.google.gson.Gson;
import java.sql.SQLException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author ryan
 */
@Path("/LoginResource")
public class LoginResource {

    /**
     *Rest request to lookup username and password combination
     * 
     * @param user The username to lookup
     * @param pass The password to lookup
     * @return Json result with LoginResponse.class structure
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String login(@QueryParam("user") String user, @QueryParam("pass") String pass) {
        LoginResponse result = new LoginResponse();
        result.setSuccess(false);
        String resultString;
        if (user != null && pass != null) {
            //Try lookup user and password
            UserModel userModel = getUserPass(user, pass);
            //see if user retrieved
            if (userModel == null || userModel.getUser() == null) {
                result.setSuccess(false);
            } else {
                //User found set success
                result.setSuccess(true);
                result.setUserModel(userModel);
                System.out.println("Login success");
            }
        }
        //Convert object to json string
        Gson gson = new Gson();
        resultString = gson.toJson(result, LoginResponse.class);
        return resultString;
    }

    /**
     * Run sql to find user and password combination
     * 
     * @param userId The username to lookup
     * @param pass The password to lookup
     * @return Usermodel, the user found or an empty Usermodel if nothing found
     */
    private UserModel getUserPass(String user, String pass) {
        Database db = new Database();
        db.connectDB();
        UserModel userModel = new UserModel();
        try {
            userModel = db.getUserPass(user, pass);
        } catch (SQLException ex) {
            System.err.println("SQL Error getting user : " + ex);
        } finally {
            db.closeDB();
        }
        return userModel;
    }

}
