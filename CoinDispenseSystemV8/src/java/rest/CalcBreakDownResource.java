package rest;

import db.Database;
import models.CalcBreakDownResponse;
import com.google.gson.Gson;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author ryan
 */
@Path("/CalcBreakDownResource")
public class CalcBreakDownResource {

    /**
     * Rest request to calculate the break down of a requested dispense amount
     *
     * @param userId The userId requesting to dispense cash
     * @param dispense The amount they wish to draw
     * @return Json result with CalcBreakDownResponse.class structure
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String calcBreakDown(@QueryParam("userId") String userId, @QueryParam("dispense") BigDecimal dispense) {
        String resultString;
        CalcBreakDownResponse result = new CalcBreakDownResponse();
        result.setSuccess(false);

        //Check paramers not null
        if (dispense != null && userId != null) {
            //Get user balance
            BigDecimal balance = getUserBalance(userId);
            if (balance != null) {
                //Need to remove decimals to not get precision errors (*100) and convert to bigint
                BigDecimal workingDispense = dispense.multiply(new BigDecimal(100));
                BigDecimal workingBalance = balance.multiply(new BigDecimal(100));
                BigInteger workingDispenseInt = workingDispense.toBigInteger();
                BigInteger workingBalanceInt = workingBalance.toBigInteger();

                //Check no remainder when divided by 5c (00.5*100=5)
                BigInteger validDenominationInt = workingDispenseInt.divide(new BigInteger("5"));
                //check no remainder and dispense amount not 0
                if (workingDispenseInt == BigInteger.ZERO || validDenominationInt.compareTo(BigInteger.ZERO) != 0) {
                    //calculate new current balance
                    BigInteger newBalance = workingBalanceInt.subtract(workingDispenseInt);
                    System.out.println("New Balance : " + newBalance.toString());
                    //chech user has enough money
                    if (dispense.compareTo(balance) <= 0) {
                        //Get break down fo amount
                        List<String> breakMe = getBreakDown(workingDispense);
                        //Update the users current balance after breakdown created
                        //updateBalance(userId, new BigDecimal(newBalance.divide(new BigInteger("100"))));
                        //set successfull response
                        result.setSuccess(true);
                        result.setBreakDown(breakMe);
                    } //User doesnt have enough balance to draw
                    else {
                        result.setSuccess(false);
                        result.setError("Insufficient funds");
                    }
                } //Remainder detected therefore we cant successfull dispense the amount a new dispense amount must be submitted
                else {
                    result.setSuccess(false);
                    result.setError("1. Valid rand denominations are R100, R50, R20 and R10.\n"
                            + "	2. Valid coin denominations are R5, R2, R1, 50c, 25c, 10c, 5c.");
                    System.out.println("1. Valid rand denominations are R100, R50, R20 and R10.\n"
                            + "	2. Valid coin denominations are R5, R2, R1, 50c, 25c, 10c, 5c.");
                }
            } else {
                result.setSuccess(false);
                result.setError("Invalid user");
            }
        }

        //Convert object to json string
        Gson gson = new Gson();
        resultString = gson.toJson(result, CalcBreakDownResponse.class);
        return resultString;
    }

    /**
     * Gets the breakdown in notes and coins for the dispense amount
     *
     * 1. Valid rand denominations are R100, R50, R20 and R10. Valid coin
     * denominations are R5, R2, R1, 50c, 25c, 10c, 5c.
     *
     * @param dispense the amount to break up
     * @return a List of note and/or coin amounts in order from highest to
     * lowest denomination eg. {"2 x R20","1 X R10", "2 x R2"} = R54
     */
    private List<String> getBreakDown(BigDecimal dispense) {
        List<String> breakMe = new ArrayList<>();
        //multiply it all into cents to no floating point problems
        //The if you can divid by the amount add the amount and its divisor to the return list 
        //and then substract the multiplied amount from the new dispense total
        int value = dispense.divide(new BigDecimal(10000), MathContext.UNLIMITED).intValue();
        if (value > 0) {
            breakMe.add(value + " x R100");
            dispense = dispense.subtract(new BigDecimal(value * 10000));
        }
        value = dispense.divideToIntegralValue(new BigDecimal(5000), MathContext.UNLIMITED).intValue();
        if (value > 0) {
            breakMe.add(value + " x R50");
            dispense = dispense.subtract(new BigDecimal(value * 5000));
        }
        value = dispense.divideToIntegralValue(new BigDecimal(2000), MathContext.UNLIMITED).intValue();
        if (value > 0) {
            breakMe.add(value + " x R20");
            dispense = dispense.subtract(new BigDecimal(value * 2000));
        }
        value = dispense.divideToIntegralValue(new BigDecimal(1000), MathContext.UNLIMITED).intValue();
        if (value > 0) {
            breakMe.add(value + " x R10");
            dispense = dispense.subtract(new BigDecimal(value * 1000));
        }
        value = dispense.divideToIntegralValue(new BigDecimal(500), MathContext.UNLIMITED).intValue();
        if (value > 0) {
            breakMe.add(value + " x R5");
            dispense = dispense.subtract(new BigDecimal(value * 500));
        }
        value = dispense.divideToIntegralValue(new BigDecimal(200), MathContext.UNLIMITED).intValue();
        if (value > 0) {
            breakMe.add(value + " x R2");
            dispense = dispense.subtract(new BigDecimal(value * 200));
        }
        value = dispense.divideToIntegralValue(new BigDecimal(100), MathContext.UNLIMITED).intValue();
        if (value > 0) {
            breakMe.add(value + " x R1");
            dispense = dispense.subtract(new BigDecimal(value * 100));
        }
        value = dispense.divideToIntegralValue(new BigDecimal(50), MathContext.UNLIMITED).intValue();
        if (value > 0) {
            breakMe.add(value + " x 50c");
            dispense = dispense.subtract(new BigDecimal(value * 50));
        }
        value = dispense.divideToIntegralValue(new BigDecimal(25), MathContext.UNLIMITED).intValue();
        if (value > 0) {
            breakMe.add(value + " x 25c");
            dispense = dispense.subtract(new BigDecimal(value * 25));
        }
        value = dispense.divideToIntegralValue(new BigDecimal(10), MathContext.UNLIMITED).intValue();
        if (value > 0) {
            breakMe.add(value + " x 10c");
            dispense = dispense.subtract(new BigDecimal(value * 10));
        }
        value = dispense.divideToIntegralValue(new BigDecimal(5), MathContext.UNLIMITED).intValue();
        if (value > 0) {
            breakMe.add(value + " x 5c");
            dispense = dispense.subtract(new BigDecimal(value * 5));
        }
        return breakMe;
    }

    /**
     * Run update balance statement on database
     *
     * @param userId The userId to update
     * @param balance The balance to update it with
     * @return true if balance was updated
     */
    private boolean updateBalance(String userId, BigDecimal balance) {
        boolean pass = false;
        Database db = new Database();
        db.connectDB();
        try {
            pass = db.updateUserBalance(userId, balance);
        } catch (SQLException ex) {
            System.err.println("SQL Error updating user balance : " + ex);
        } finally {
            db.closeDB();
        }
        return pass;
    }

    /**
     * Get the users current balance
     * 
     * @param userId
     * @return The users current Balance or a null if not found
     */
    private BigDecimal getUserBalance(String userId) {
        BigDecimal balance = null;
        Database db = new Database();
        db.connectDB();
        try {
            balance = db.getUserBalance(userId);
        } catch (SQLException ex) {
            System.err.println("SQL Error getting user balance : " + ex);
        } finally {
            db.closeDB();
        }

        db.closeDB();
        return balance;
    }

}
