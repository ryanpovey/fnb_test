package db;

import models.UserModel;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author ryan
 */
public class Database {
    
    //Edit Path below
    //Remeber to include final slash and set to full absolute path
    String dbInstallPath = "/home/rpovey/";

    
    //Connection details for database
    Connection con;
    String driverClass = "org.h2.Driver";
    String username = "fnb";
    String host;
    String dbname;
    int port;
    String pw = "sMil3y-k1Ng #_n0_T3eTh";
    String url = "jdbc:h2:file:" + dbInstallPath + "fnb;CIPHER=AES;AUTO_SERVER=TRUE;MVCC=TRUE;DATABASE_TO_UPPER=FALSE";

    public Database() {
    }

    /**
     * Connects to Database based on object driver
     *
     */
    public void connectDB() {
        // Load the JDBC driver
        try {
            Class.forName(this.driverClass);
        } catch (ClassNotFoundException e) {
            System.err.println("Error loading driver: " + e);
        }
        try {
            //Get database connection
            this.con = DriverManager.getConnection(this.url, this.username, this.pw);
            System.out.println("Connected!!!");
        } catch (SQLException ex) {
            System.err.println("Error Connecting to DB: " + ex);
        }
    }

    /**
     * Closes the connection to the Database
     *
     */
    public void closeDB() {
        try {
            // Close connection
            this.con.close();
        } catch (SQLException ex) {
            System.err.println("Error Closing DB connection: " + ex);
        }
    }

    /**
     * Does a lookup on the database for user and password combination
     *
     * @param user
     * @param pass
     * @return UserModel, the user model found or an empty user model if not
     * found
     * @throws java.sql.SQLException
     */
    public UserModel getUserPass(String user, String pass) throws SQLException {
        UserModel userModel = new UserModel();
        PreparedStatement getUser = con.prepareStatement("select * from users where username = ? and password = ?");
        getUser.setString(1, user);
        getUser.setString(2, pass);

        ResultSet result = getUser.executeQuery();

        //if ret result back popualte result model
        while (result.next()) {
            userModel.setUserId(result.getString("user_id"));
            userModel.setUser(result.getString("username"));
            userModel.setBalance(result.getBigDecimal("current_balance"));
        }
        return userModel;
    }

    /**
     * Updates the users balance based on the UserId sent
     *
     *
     * @param userId The userId to update
     * @param balance The new user balance
     * @return true if balance was updated
     */
    public boolean updateUserBalance(String userId, BigDecimal balance) throws SQLException {
        boolean updateResult = false;
        PreparedStatement getUser = con.prepareStatement("update users set current_balance = ? where user_id = ?");
        getUser.setBigDecimal(1, balance);
        getUser.setString(2, userId);
        updateResult = getUser.execute();
        return updateResult;
    }


    /**
     * Get the users balance based on UserId Lookup
     * 
     * @param userId the User to find
     * @return The users current balance
     * @throws SQLException
     */
    public BigDecimal getUserBalance(String userId) throws SQLException {
        BigDecimal balance = null;
        PreparedStatement getUser = con.prepareStatement("select * from users where user_id = ?");
        getUser.setString(1, userId);

        ResultSet result = getUser.executeQuery();
        //if get result set balance
        while (result.next()) {
            balance = result.getBigDecimal("current_balance");
        }

        return balance;
    }

    
    
    
    
    public boolean insertTestUser() {
        boolean insertResult = false;
        try {
            PreparedStatement insertTestUser = con.prepareStatement("insert into users(user_id,username,password,current_balance) values ('00TEST','testLogin','pass',100)");
            insertResult = insertTestUser.execute();

        } catch (SQLException ex) {
            System.err.println("Error createing prepared statement : " + ex);
            return false;
        }
        return insertResult;
    }

    public boolean deleteTestUser() {
        boolean deleteResult = false;
        try {
            PreparedStatement deleteTestUser = con.prepareStatement("delete from users where user_id = '00TEST'");
            deleteResult = deleteTestUser.execute();

        } catch (SQLException ex) {
            System.err.println("Error createing prepared statement : " + ex);
            return false;
        }
        return deleteResult;
    }
    
    
}
