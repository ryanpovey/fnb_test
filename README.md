The Cash Dispense project :

IDE : Netbeans 8.2
Java Platform : jdk1.8.0_131
App server used : Payara Server 4.1.1.161.1 (Also tested on Glassfish 4.1.1)


This project consists of the Client application (Go) which connects to the Webservices and database services (CoinDispenseSystemV8).

To run the project : 

1. Copy file fnb.h2.db to a new Director
2. Edit Class Database.java in the db package in CoinDispenseSystemV8 source and set the variable dbInstallPath to the directory you copied fnb.h2.db to.
    Eg. String dbInstallPath = "C:/New_Folder/";
2. Build and deploy CoinDispenseSystemV8 to application server.
3. Run the Go Desktop app for login screen.
4. Login with credentials :
    Username = ryan
    Password = pass


Go :

Desktop app with navigation and Webservice requests.


CoinDispenseSystemV8 :

Produces Webservices that return json and connect to an h2 database